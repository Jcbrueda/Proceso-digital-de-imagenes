/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.advance.practica7;

import com.filters.main.Editor;
import com.filters.service.Dither;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jcbru
 */
@ManagedBean(name="dither")
@ViewScoped
public class DitherController implements Serializable {
    
    public static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{editor}")
    private Editor editor;
    
    private int mode;
    
    public DitherController() {
        this.mode = 0;
    }

    public Editor getEditor() {
        return editor;
    }

    public void setEditor(Editor editor) {
        this.editor = editor;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
    
    public void aplicar() {
        if (mode == 3)
            editor.setImg(Dither.difusion(editor.getImg2()));
        else
            editor.setImg(Dither.apply(mode,editor.getImg2()));
    }
}

