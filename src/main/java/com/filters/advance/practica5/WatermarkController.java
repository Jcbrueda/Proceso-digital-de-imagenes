/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.advance.practica5;

import com.filters.main.Editor;
import com.filters.service.Watermark;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jcbru
 */
@ManagedBean(name="watermark")
@ViewScoped
public class WatermarkController implements Serializable {
    
    public static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{editor}")
    private Editor editor;
    
    private String filter;
    
    public WatermarkController() {
        this.filter = "remove";
    }

    public Editor getEditor() {
        return editor;
    }

    public void setEditor(Editor editor) {
        this.editor = editor;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
    
    public void aplicar() {
        if (filter.equals("remove")) {
            editor.setImg(Watermark.remove(editor.getImg2()));
            return;
        }
        editor.setImg(Watermark.add(editor.getImg2()));
    }
}
