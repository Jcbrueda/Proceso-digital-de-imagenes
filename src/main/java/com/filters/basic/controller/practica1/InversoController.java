/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.basic.controller.practica1;

import com.filters.main.Editor;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jcbru
 */
@ManagedBean(name = "inverso")
@ViewScoped
public class InversoController implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{editor}")
    private Editor editor;

    public InversoController() {

    }

    public Editor getEditor() {
        return editor;
    }

    public void setEditor(Editor editor) {
        this.editor = editor;
    }

    public void aplicar() {
        editor.setImg(inverso(editor.getImg2()));
    }

    /* Filtro de una imagen que invierte los colores de una imagen.
     * @param img es el buffer de la imagen original.
     * @return Devuelve un buffer con los colores invertidos.
     */
    public static BufferedImage inverso(BufferedImage img) {
        // saca la medida ancho y alto del buffer que recibe
        int w = img.getWidth();
        int h = img.getHeight();

        // crea un buffer local para no alterar el buffer que recibe
        BufferedImage bim = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        // recorre los pixeles del buffer 
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                // obtiene el color del pixel
                Color c = new Color(img.getRGB(i, j));
                // invierte los colores
                c = new Color(0xFF - c.getRed(), 0xFF - c.getGreen(), 0xFF - c.getBlue());
                // asigna el color invertido en el pixel de buffer
                bim.setRGB(i, j, c.getRGB());
            }
        }
        return bim;
    }
}
