/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.basic.controller.practica2;

/**
 *
 * @author jcbru
 */

import java.io.Serializable;
import com.filters.main.Convolution;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import com.filters.main.Editor;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author jcbru
 */
@ManagedBean(name="motionBlur")
@SessionScoped
public class motionBlurController implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{editor}")
    private Editor editor;
    
    private double[][] m = {{1, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 1, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 1, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 1, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 1, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 1, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 1, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 1, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 1}};
    
    public motionBlurController() {
        
    }

    public Editor getEditor() {
        return editor;
    }

    public void setEditor(Editor editor) {
        this.editor = editor;
    }
    
    public void aplicar() {
        editor.setImg(Convolution.convolution(editor.getImg2(), m, 1.0 / 9.0, 0));
    }
}
