/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Beans/Bean.java to edit this template
 */
package com.filters.basic.controller.practica2;

import java.io.Serializable;
import com.filters.main.Convolution;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import com.filters.main.Editor;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jcbru
 */
@ManagedBean(name="blur")
@SessionScoped
public class blurController implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{editor}")
    private Editor editor;
    
    private double[][] m = {{0,0,1,0,0},
                            {0,1,1,1,0},
                            {1,1,1,1,1},
                            {0,1,1,1,0},
                            {0,0,1,0,0}};
    
    public blurController() {
        
    }

    public Editor getEditor() {
        return editor;
    }

    public void setEditor(Editor editor) {
        this.editor = editor;
    }
    
    public void aplicar() {
        editor.setImg(Convolution.convolution(editor.getImg2(), m, 1.0 / 13.0, 0));
    }
}
