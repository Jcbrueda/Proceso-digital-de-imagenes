/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.basic.controller.practica2;

import com.filters.main.Editor;
import com.filters.main.Convolution;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jcbru
 */
@ManagedBean(name = "mean")
@ViewScoped
public class MeanController implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{editor}")
    private Editor editor;
    
    private double m [][]= {{1,1,1},
                            {1,1,1},
                            {1,1,1}};
    
    private double factor = (1.0/9.0);

    /**
     * @return the editor
     */
    public Editor getEditor() {
        return editor;
    }

    /**
     * @param editor the editor to set
     */
    public void setEditor(Editor editor) {
        this.editor = editor;
    }
    
    public void aplicar(){
        editor.setImg(Convolution.convolution(editor.getImg2(), m, factor , 0.0));
    }
}
