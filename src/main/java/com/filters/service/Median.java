/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.service;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author jcbru
 */
public class Median {

    public static BufferedImage median(BufferedImage bimg) {

        int width = bimg.getWidth();
        int height = bimg.getHeight();
        int size = 3;
        int half = size / 2;
        int r[];
        int g[];
        int b[];
        int xi;
        int xf;
        int yi;
        int yf;
        int px;
        int py;

        Color coloresOriginales[][] = new Color[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                coloresOriginales[i][j] = new Color(bimg.getRGB(i, j));
            }
        }

        for (int i = 0; i < width; i++) {

            // Se definen los limites horizontales de la matriz segun la posicion del pixel actual.
            if (i < half) {
                xi = half - i;
            } else {
                xi = 0;
            }

            if (width - i <= half) {
                xf = half + width - i;
            } else {
                xf = size;
            }

            for (int j = 0; j < height; j++) {
                // Se definen los limites verticales de la matriz segun la posicion del pixel actual.
                if (j < half) {
                    yi = half - j;
                } else {
                    yi = 0;
                }

                if (width - j <= half) {
                    yf = half + height - j;
                } else {
                    yf = size;
                }
                
                r = new int[(xf - xi) * (yf - yi)];
                g = new int[r.length];
                b = new int[r.length];
                
                for (int k = 0; (k + xi) < xf; k++) {
                    for (int l = 0; (l + yi) < yf; l++) {

                        px = i - half;
                        py = j - half;

                        r[l + (yf - yi) * k] = coloresOriginales[px + k + xi][py + l + yi].getRed();
                        g[l + (yf - yi) * k] = coloresOriginales[px + k + xi][py + l + yi].getGreen();
                        b[l + (yf - yi) * k] = coloresOriginales[px + k + xi][py + l + yi].getBlue();
                    }
                }
                bimg.setRGB(i, j, new Color(mediana(r), mediana(g), mediana(b)).getRGB());
            }
        }

        return bimg;
    }

    private static int mediana(int... x) {
        int xn = x.length;
        int m = -1;
        for (int i = 0, j = 0; i <= (xn / 2); i++, j = i) {
            for (int k = i + 1; k < xn; k++) {
                if (x[j] > x[k]) {
                    j = k;
                }
            }

            m = x[j];
            x[j] = x[i];
            x[i] = m;
        }

        if ((xn % 2) == 0) {
            return (m + x[xn / 2 - 1]) / 2;
        } else {
            return m;
        }
    }
}
