/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.intermediate.controller.practica3;

import com.filters.main.Editor;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.Part;
import com.filters.service.Letters;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author jcbru
 */
@ManagedBean(name = "letters")
@ViewScoped
public class LetterControler implements Serializable {

    public static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{editor}")
    private Editor editor;

    private Part file;
    private String fileName;
    private String text;
    private int size;
    private boolean sg;

    public LetterControler() {
        this.text = "MNH#QUAD0Y2$%+.@B/";
        this.file = null;
        this.size = 10;
        this.sg = false;
    }

    /**
     * @return the editor
     */
    public Editor getEditor() {
        return editor;
    }

    /**
     * @param editor the editor to set
     */
    public void setEditor(Editor editor) {
        this.editor = editor;
    }

    /**
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Part file) {
        this.file = file;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        text = text.trim().toLowerCase();
        if ("".equals(text) || text == null) {

        } else {
            this.text = text;
        }
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the sg
     */
    public boolean isSg() {
        return sg;
    }

    /**
     * @param sg the sg to set
     */
    public void setSg(boolean sg) {
        this.sg = sg;
    }

    public void aplicar() {
        String[] chars = text.split("");
        List<String> letters = new ArrayList<>(Arrays.asList(chars));
        letters.add(" ");
        editor.setImg(Letters.apply(letters, "Cousine", size, sg, editor.getImg2()));
    }

    public void sizeUp() {
        size += size < 24 ? 1 : 0;
    }

    public void sizeDown() {
        size -= size > 6 ? 1 : 0;
    }

    public void uploadFile() throws IOException {
        FacesContext current = FacesContext.getCurrentInstance();
        this.text = "";
        fileName = getFileNameL(file);
        InputStream input = file.getInputStream();
        
        try (BufferedReader br = new BufferedReader(new InputStreamReader(input))) {
            String line;
            while ((line = br.readLine()) != null) {
                text += line.trim().toUpperCase();
            }
        } catch (Exception ex) {
            current.addMessage(null,
                    new FacesMessage(null, "Oops!!, Algo salió mal", ex.getMessage()));
        }
    }

    private String getFileNameL(Part part) {
        String fileNameL = "";
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                fileNameL = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return fileNameL.substring(fileNameL.lastIndexOf('/') + 1).substring(fileNameL.lastIndexOf('\\') + 1); // MSIE fix. } } return null; }
    }
}
