/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.filters.main;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author jcbru
 */
public class Convolution {

    public static BufferedImage convolution(BufferedImage bimg,
            double matrix[][], double factor, double bias) {

        int height = bimg.getHeight();
        int width = bimg.getWidth();
        int size = matrix.length;
        int half = size / 2;
        double r;
        double g;
        double b;
        int yi;
        int yf;
        int xi;
        int xf;
        int py;
        int px;
        double value;
        
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        Color coloresOriginales[][] = new Color[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                coloresOriginales[i][j] = new Color(bimg.getRGB(i, j));
            }
        }

        for (int i = 0; i < width; i++) {

            // Se definen los limites horizontales de la matriz segun la posicion del pixel actual.
            if (i < half) {
                xi = half - i; 
            } else {
                xi = 0;
            }

            // Se definen los limites horizantales de la matriz segun la posicion del pixel actual.
            if (width - i <= half) {
                xf = half + width - i;
            } else {
                xf = size;
            }

            for (int j = 0; j < height; j++) {
                r = 0;
                g = 0;
                b = 0;

                // Se definen los limites verticales de la matriz segun la posicion del pixel actual.
                if (j < half) {
                    yi = half - j;
                } else {
                    yi = 0;
                }

                if (height - j <= half) {
                    yf = half + height - j;
                } else {
                    yf = size;
                }

                for (int k = xi; k < xf; k++) {
                    px = i - half;

                    for (int l = yi; l < yf; l++) {
                        py = j - half;
                        
                        value = matrix[k][l];

                        r += (coloresOriginales[px + k][py + l].getRed() * value);
                        g += (coloresOriginales[px + k][py + l].getGreen() * value);
                        b += (coloresOriginales[px + k][py + l].getBlue() * value);
                    }
                }

                r = r * factor + bias;
                g = g * factor + bias;
                b = b * factor + bias;

                bi.setRGB(i, j, new Color(normalize((int) r), normalize((int) g), normalize((int) b)).getRGB());
            }
        }

        return bi;

    }

    private static int normalize(int n) {
        if (n < 0) {
            return 0;
        } else if (n <= 255) {
            return n;
        } else {
            return 255;
        }
    }

}
