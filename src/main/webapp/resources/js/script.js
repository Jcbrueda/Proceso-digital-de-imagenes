/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */

/*
$(document).ready(function () {
    $('.dropdown-submenu a.test').on("click", function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
});
*/

$(".custom-file-input").change(function (e) {
    var fileName = e.target.files[0].name;
    if (fileName !== null) {
        $(this).parent(".custom-file").children("label").text(fileName);
    }
});

